﻿using UnityEngine;

namespace Assets.Scripts.Camera
{
    public class Background : MonoBehaviour
    {
        public float ScrollSpeed = 1.59f;
        public float TileSizeZ = 98f;

        private Vector3 _startPosition;

        private void Start()
        {
            _startPosition = transform.position;
            transform.FindChild("background").GetComponent<SpriteRenderer>().enabled = true;
        }

        private void Update()
        {
            var newPosition = Mathf.Repeat(Time.time*ScrollSpeed, TileSizeZ);
            transform.position = _startPosition + Vector3.right*newPosition;
            
        }
    }
}