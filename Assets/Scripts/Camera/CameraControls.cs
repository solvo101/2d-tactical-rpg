﻿using UnityEngine;

namespace Assets.Scripts.Camera
{
    internal class CameraControls : MonoBehaviour
    {
        private FollowTarget _followTarget;

        private void Start()
        {
            _followTarget = FindObjectOfType<FollowTarget>();
        }

        private void Update()
        {
            if (_followTarget.Target != null) return;
            if (Input.GetMouseButton(2))
            {
                GetInputForAxis(Vector3.up, "Mouse Y");
                GetInputForAxis(Vector3.right, "Mouse X");
            }

            var move = camera.orthographicSize;
            var speed = Input.GetAxis("Mouse ScrollWheel");
            move -= speed*2.25f;
            camera.orthographicSize = move;
        }

        private void GetInputForAxis(Vector3 dir, string axis)
        {
            var move = 0.0f;
            var speed = Input.GetAxis(axis);
            move -= speed*1.5f;
            if (speed != 0.0f)
            {
                transform.Translate(dir*move);
            }
        }
    }
}