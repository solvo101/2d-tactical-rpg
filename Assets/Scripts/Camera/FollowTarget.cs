﻿using UnityEngine;

namespace Assets.Scripts.Camera
{
    public class FollowTarget : MonoBehaviour
    {
        public float Smooth = 5.0f;
        public Transform Target;
        private bool _shouldStop = true;

        private void Update()
        {
            if (Target == null) return;
            transform.position = Vector3.Lerp(
                transform.position, new Vector3(Target.position.x, Target.position.y, -10f),
                Time.deltaTime*Smooth);
            if (!(Vector2.Distance(Target.position, transform.position) < .07f) || !_shouldStop) return;
            _shouldStop = false;
            Target = null;
        }

        public void MoveToTargetAndStop(Transform target)
        {
            Target = target;
            _shouldStop = true;
        }
    }
}