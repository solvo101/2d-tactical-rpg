﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Items
{
    public abstract class Item : MonoBehaviour
    {
        public HUDManager HUDManager { get; private set; }

        void Start ()
        {
            HUDManager = FindObjectOfType<HUDManager>();
        }
    
        void Update () 
        {
    
        }
    }
}
