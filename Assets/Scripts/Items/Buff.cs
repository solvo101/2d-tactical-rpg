﻿using Assets.Scripts.Units;

namespace Assets.Scripts.Items
{
    public abstract class Buff : Item
    {
        protected Unit Unit;

        public void Activate(Unit unit)
        {
            Unit = unit;
            ApplyEffects();
            HUDManager.ShowUnitTooltip(unit);
            Destroy(gameObject);
        }

        protected abstract void ApplyEffects();
    }
}
