﻿using System.Collections.Generic;
using Assets.Scripts.Units.Attributes.BuffEffects;

namespace Assets.Scripts.Items.Buffs
{
    public class DamageBuff : Buff
    {
        protected override void ApplyEffects()
        {
            var buffEffects = new List<BuffEffect>
            {
                new DamageEffect
                {
                    Duration = 3,
                    Value = 2
                }
            };
            Unit.ApplyBuffEffects(buffEffects);
        }
    }
}
