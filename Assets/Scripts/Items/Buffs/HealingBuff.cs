﻿namespace Assets.Scripts.Items.Buffs
{
    public class HealingBuff : Buff
    {
        protected override void ApplyEffects()
        {
            Unit.Heal(10);
        }
    }
}
