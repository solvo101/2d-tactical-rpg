﻿using System.Collections.Generic;
using Assets.Scripts.Units.Attributes.BuffEffects;

namespace Assets.Scripts.Items.Buffs
{
    public class BerserkerBuff : Buff
    {
        protected override void ApplyEffects()
        {
            Unit.Heal(3);
            var buffEffects = new List<BuffEffect>
            {
                new DamageEffect
                {
                    Duration = 3,
                    Value = 1,
                },
                new MovementEffect
                {
                    Duration = 3,
                    Value = 1
                },
            };
            Unit.ApplyBuffEffects(buffEffects);
        }
    }
}
