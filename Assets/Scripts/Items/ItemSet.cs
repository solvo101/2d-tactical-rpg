﻿using UnityEngine;

namespace Assets.Scripts.Items
{
    public class ItemSet : ScriptableObject
    {
        public Item[] Items = new Item[0];
    }
}