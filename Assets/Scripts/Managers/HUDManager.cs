﻿using System.Linq;
using Assets.Scripts.GUI;
using Assets.Scripts.Units;
using Assets.Scripts.Units.Attributes.BuffEffects;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class HUDManager : MonoBehaviour
    {
        public static Color HealthColor { get { return new Color(1f, 0.705f, 0.705f); } }
        public static Color ManaColor { get { return new Color(0.337f, 0.698f, 1f); } }

        public Texture2D DamageIcon, MovementIcon;

        public GameObject Menu, SubMenu, StartButton, UnitTooltip, HighlightTooltip;
        private Transform[] _buffs = new Transform[5];
        private HUDElement _highlightTooltip;
        private MenuHUDElement _menu;
        private SubMenuHUDElement _subMenu;
        private HUDElement _start;
        private HUDElement _unitTooltip;

        private void Start()
        {
            _menu = Menu.GetComponent<MenuHUDElement>();
            _subMenu = SubMenu.GetComponent<SubMenuHUDElement>();
            _start = StartButton.GetComponent<HUDElement>();
            _unitTooltip = UnitTooltip.GetComponent<HUDElement>();
            _highlightTooltip = HighlightTooltip.GetComponent<HUDElement>();
        }

        public void ShowMenu()
        {
            _menu.Show();
        }
        
        public void HideMenu()
        {
            if (_menu == null)
            {
                Start();
            }
            _menu.Hide();
        }

        public void ShowSubMenu(Unit unit, UnitActions action)
        {
            _subMenu.Show(unit, action);
        }

        public void HideSubMenu()
        {
            _subMenu.Hide();
        }

        public void HideStart()
        {
            _start.Hide();
        }

        public void ShowUnitTooltip(Unit unit)
        {
            UpdateTooltip(UnitTooltip, unit);
            _unitTooltip.Show();
        }

        public void HideUnitTooltip()
        {
            _unitTooltip.Hide();
        }

        public void ShowHighlightTooltip(Unit unit)
        {
            UpdateTooltip(HighlightTooltip, unit);
            _highlightTooltip.Show();
        }

        public void HideHighlightTooltip()
        {
            _highlightTooltip.Hide();
        }

        private static Texture2D TextureFromSprite(Sprite sprite)
        {
            if (sprite.rect.width == sprite.texture.width) return sprite.texture;
            var newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            var newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                (int)sprite.textureRect.y,
                (int)sprite.textureRect.width,
                (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }

        private void UpdateTooltip(GameObject tooltip, Unit unit)
        {
            var tooltipTransform = tooltip.transform;
            var nameText = tooltipTransform.FindChild("UnitName").gameObject.GetComponent<GUIText>();
            nameText.text = unit.UnitName;
            nameText.color = unit is Player ? Color.green : Color.yellow;

            SetAttribute(unit.Damage, unit.DamageMod, "UnitDamage", tooltipTransform, Color.white);
            SetAttribute(unit.MoveDistance, unit.MoveDistanceMod, "UnitMovement", tooltipTransform, Color.white);
            SetAttribute(unit.MaxHealth, unit.MaxHealthMod, "UnitMaxHealth", tooltipTransform, HealthColor);
            SetAttribute(unit.Health, unit.HealthMod, "UnitHealth", tooltipTransform, HealthColor);
            SetAttribute(unit.MaxMana, unit.MaxManaMod, "UnitMaxMana", tooltipTransform, ManaColor);
            SetAttribute(unit.Mana, unit.ManaMod, "UnitMana", tooltipTransform, ManaColor);

            tooltipTransform.FindChild("UnitPicture").gameObject.GetComponent<GUITexture>().texture =
                TextureFromSprite(((SpriteRenderer)unit.renderer).sprite);

            var buffHolder = tooltipTransform.FindChild("BuffHolder");
            for (var i = 0; i < 5; i++)
            {
                _buffs[i] = buffHolder.GetChild(i);
                _buffs[i].gameObject.SetActive(false);//hide all buffs to only show the ones we need
            }

            var activeBuffs = unit.GetActiveBuffs();
            if (activeBuffs == null || !activeBuffs.Any()) return;

            var buffIndex = 0;
            foreach (var buff in activeBuffs)
            {
                var buffView = _buffs[buffIndex];
                buffView.gameObject.SetActive(true);

                SetBuffAmount(buff, buffView);

                buffView.transform.FindChild("BuffDuration").gameObject.GetComponent<GUIText>().text = buff.Duration.ToString();

                SetBuffIcon(buff, buffView);
                buffIndex++;
            }
        }

        private void SetAttribute(int stat, int statMod, string statName, Transform tooltipTransform, Color defaultColor)
        {
            var text = tooltipTransform.FindChild(statName).gameObject.GetComponent<GUIText>();
            text.text = stat.ToString();
            if (statMod < 0)
                text.color = Color.red;
            else if (statMod == 0)
            {
                text.color = defaultColor;
            }
            else if (statMod > 0)
            {
                text.color = Color.green;
            }
        }

        private void SetBuffIcon(BuffEffect buff, Transform buffView)
        {
            Texture2D statTexture = null;
            if (buff is DamageEffect)
                statTexture = DamageIcon;
            else if (buff is MovementEffect)
                statTexture = MovementIcon;

            buffView.transform.FindChild("BuffIcon").gameObject.GetComponent<GUITexture>().texture = statTexture;
        }

        private static void SetBuffAmount(BuffEffect buff, Transform buffView)
        {
            var buffAmount = buff.Value;
            var buffAmountText = buffAmount.ToString();
            if (buffAmount > 0)
                buffAmountText = "+" + buffAmountText;
            else
                buffAmountText = "-" + buffAmountText;
            buffView.transform.FindChild("BuffAmount").gameObject.GetComponent<GUIText>().text = buffAmountText;
        }
    }
}