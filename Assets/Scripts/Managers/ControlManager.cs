﻿using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class ControlManager : MonoBehaviour
    {
        private GameManager _gameManager;

        // Use this for initialization
        private void Start()
        {
            _gameManager = FindObjectOfType<GameManager>();
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKeyDown("left alt"))
                _gameManager.ToggleHealthIndicator();
        }
    }
}