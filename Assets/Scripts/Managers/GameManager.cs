﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using Assets.Scripts.Camera;
using Assets.Scripts.Map;
using Assets.Scripts.Units;
using Assets.Scripts.Units.Skills;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class GameManager : MonoBehaviour
    {
        public List<UnitActions> CompletedActions;
        public UnitActions CurrentAction;
        public Unit CurrentUnit;
        public GameObject EnemyPrefab;
        public GameObject PlayerPrefab;
        private List<Tile> _choices;
        private FollowTarget _followTarget;
        private Pathfinder _pathfinder;
        private Queue<Unit> _turnOrderQueue;
        private Skill _selectedSkill;
        public bool HealthIndicatorsActive { get; private set; }
        public bool LevelOver { get; set; } //flag the manager to get a new level ready.
        public bool PlayerDied { get; set; } //did the player die?

        public bool IsPlayerTurn
        {
            get { return CurrentUnit is Player; }
        }

        public MapManager MapManager { get; private set; }
        public HUDManager HUDManager { get; private set; }

        private void Start()
        {
            MapManager = FindObjectOfType<MapManager>();
            HUDManager = FindObjectOfType<HUDManager>();
            _followTarget = FindObjectOfType<FollowTarget>();
            HUDManager.HideMenu(); //Don't show the menu initially
            HUDManager.HideSubMenu(); //Don't show the subMenu initially
            HUDManager.HideUnitTooltip();
            HUDManager.HideHighlightTooltip();
            _pathfinder = new Pathfinder(MapManager);
            _turnOrderQueue = new Queue<Unit>();
        }

        public void BeginGame()
        {
            LevelOver = false;
            PlayerDied = false;
            HUDManager.HideStart();
            
            _turnOrderQueue.Enqueue(MapManager.GetPlayer());
            MapManager.GetEnemies().ForEach(e => _turnOrderQueue.Enqueue(e));

            NextTurn();
        }

        private void NextTurn()
        {
            if (LevelOver)
            {
                EndLevel();
                return;
            }

            CompletedActions.Clear();
            do
            {
                CurrentUnit = _turnOrderQueue.Dequeue();
            } while (CurrentUnit == null);

            _turnOrderQueue.Enqueue(CurrentUnit);
            if (IsPlayerTurn)
            {
                _followTarget.MoveToTargetAndStop(CurrentUnit.transform);
                HUDManager.ShowMenu();
            }
            else
            {
                _followTarget.Target = CurrentUnit.transform;
            }
            HUDManager.ShowUnitTooltip(CurrentUnit);
            CurrentUnit.BeginTurn();
        }

        public void TileClicked(Tile tile)
        {
            HUDManager.HideMenu();
            switch (CurrentAction)
            {
                case UnitActions.Skills:
                    {
                        UseSkillAt(tile, _selectedSkill);
                        break;
                    }

                case UnitActions.Move:
                    {
                        if (tile.IsOccupied) return;
                        MoveCurrentUnit(tile);
                        break;
                    }
            }
        }

        public void MenuMoveClicked()
        {
            CurrentAction = UnitActions.Move;
            HUDManager.HideSubMenu();
            RemoveBoundaries();
            DrawMoveBoundaries();
        }

        public void MenuSkillsClicked()
        {
            CurrentAction = UnitActions.Skills;
            RemoveBoundaries();
            HUDManager.ShowSubMenu(CurrentUnit, CurrentAction);
        }

        public void MenuSkillClicked(Skill skill)
        {
            _selectedSkill = skill;
            RemoveBoundaries();
            DrawSkillBoundaries(skill);
        }

        public void MenuEndClicked()
        {
            HUDManager.HideMenu();
            HUDManager.HideSubMenu();
            RemoveBoundaries();
            EndTurn();
        }

        public void MoveCurrentUnit(Tile destinationTile)
        {
            if (IsPlayerTurn && !destinationTile.IsChoice) return;

            if (IsPlayerTurn)
            {
                RemoveBoundaries();
            }

            //find a path to the destination
            var path = _pathfinder.CreatePath(CurrentUnit.GetLocation(), destinationTile);

            if (path == null)
            {
                EndTurn();
                return;
            }

            if (!IsPlayerTurn)
            {
                var pathLength = path.Count;

                if (pathLength == 1)
                {
                    //the enemy is already there (The one node is just the players location. Just having one node means the enemy is next to the player)
                    EndTurn();
                    return;
                }

                //trim the enemies' path to just their moveDistance
                if (CurrentUnit.MoveDistance < pathLength)
                    path = path.GetRange(0, CurrentUnit.MoveDistance);

                if (path.Last().IsOccupied)
                {
                    path.RemoveAt(path.Count - 1); // remove the last node if the player is standing on it
                }
            }
            else
                _followTarget.Target = CurrentUnit.transform;

            path.Last().IsDestination = true;
            CurrentUnit.SetNewDestination(new Queue<Tile>(path));
        }

        public void UseSkillAt(Tile tile, Skill skill)
        {
            var target = tile.GetOccupyingUnit();
            if (target == null) throw new Exception("Player tried to attack a tile without a unit.");
            _followTarget.MoveToTargetAndStop(CurrentUnit.transform);
            RemoveBoundaries();
            CurrentUnit.UseSkill(target, skill);
        }

        public void DoneUsingSkill()
        {
            if (IsPlayerTurn)
            {
                CompletedActions.Add(UnitActions.Skills);
                HUDManager.HideSubMenu();
                HUDManager.ShowMenu();
            }
            else
            {
                //it was the enemies turn and since they don't move after attacking, end the turn
                NextTurn();
            }
        }

        public void DoneMoving(Tile tile)
        {
            CancelMove(tile);
            CompletedActions.Add(UnitActions.Move);
            if (!IsPlayerTurn && !CompletedActions.Contains(UnitActions.Skills))
            {
                ((Enemy)CurrentUnit).DecideToAttack();
                return;
            }
            _followTarget.Target = null;
            HUDManager.ShowMenu();
        }

        public Player GetPlayer()
        {
            return _turnOrderQueue.First(u => u is Player) as Player;
        }

        //gets all the tiles in range of the given tile and distance from that tile
        public List<Tile> GetTilesInRange(Tile source, int distance)
        {
            var position = source.GetGridPosition();
            var tiles = new List<Tile>();
            for (var x = (int)(position.x - distance); x <= distance + position.x; x++)
            {
                for (var y = (int)(position.y - distance); y <= distance + position.y; y++)
                {
                    if (x < 0 || y < 0 || (Math.Abs(position.x - x) + Math.Abs(position.y - y)) > distance)
                        continue;
                    var tile = MapManager.FindTileAt(x, y);
                    if (tile != null)
                        tiles.Add(tile);
                }
            }
            return tiles;
        }

        private void EndLevel()
        {
            var index = Application.loadedLevel + 1;
            if (PlayerDied || index == Application.levelCount)
                index = 0;
            Application.LoadLevel(index);
        }

        private void CancelMove(Tile tile)
        {
            tile.IsDestination = false;
            tile.transform.renderer.material.color = Color.white;
        }

        private void DrawMoveBoundaries()
        {
            if (_choices != null) throw new Exception("_choices didn't get cleared before trying to draw skill boundaries!");

            var location = CurrentUnit.GetLocation();
            var moveDistance = CurrentUnit.MoveDistance;
            var tilesInRange = GetTilesInRange(location, moveDistance);

            _choices = new List<Tile>();
            foreach (var tile in tilesInRange.Where(tile => tile.IsAvailable && _pathfinder.IsPathAvailable(location, tile, moveDistance)))
            {
                tile.IsChoice = true;
                _choices.Add(tile);
            }
        }

        private void DrawSkillBoundaries(Skill skill)
        {
            if (_choices != null) throw new Exception("_choices didn't get cleared before trying to draw skill boundaries!");
            var location = CurrentUnit.GetLocation();
            var range = skill.Range;

            _choices = new List<Tile>();

            GetTilesInRange(location, range).ForEach(AddChoiceTileIfUnitExists);

            if (skill.CanTargetSelf)
            {
                location.IsChoice = true;
                _choices.Add(location);
            }
        }

        private void AddChoiceTileIfUnitExists(Tile tile)
        {
            if (tile == null || !tile.IsOccupiedByEnemy) return;
            _choices.Add(tile);
            tile.IsChoice = true;
        }

        private void RemoveBoundaries()
        {
            if (_choices == null) return;
            foreach (var tile in _choices)
            {
                tile.IsChoice = false;
            }
            _choices = null;
        }

        public void EndTurn()
        {
            _followTarget.Target = null;
            NextTurn();
        }

        public void ShowHighlightTooltip(Unit occupyingUnit)
        {
            HUDManager.ShowHighlightTooltip(occupyingUnit);
        }

        public void HideHighlightTooltip()
        {
            HUDManager.HideHighlightTooltip();
        }

        public void ToggleHealthIndicator()
        {
            var units = _turnOrderQueue.ToList();
            HealthIndicatorsActive = !HealthIndicatorsActive;
            foreach (var unit in units.Where(unit => unit != null))
            {
                unit.ShowHealthIndicator(HealthIndicatorsActive);
            }
        }

        public void CheckGameStatus()
        {
            //Are there any enemies left?
            if (_turnOrderQueue.OfType<Enemy>().All(e => e.Health <= 0))
            {
                LevelOver = true;
            }
            //Did the player die?
            else if (_turnOrderQueue.OfType<Player>().All(e => e.Health <= 0))
            {
                LevelOver = true;
                PlayerDied = true;
            }
        }

        public bool IsMyTurn(Unit unit)
        {
            return CurrentUnit == unit;
        }

        public Unit GetCurrentUnit()
        {
            return CurrentUnit;
        }
    }

    public enum UnitActions
    {
        Skills,
        Move
    };
}