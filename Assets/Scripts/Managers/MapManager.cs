﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Items;
using Assets.Scripts.Map;
using Assets.Scripts.Units;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class MapManager : MonoBehaviour
    {
        public Color LineColor = Color.white;
        public static int TileSize { get { return 1; } }

        public int MapWidth;
        public int MapHeight;

        public Transform TilePrefab;
        public TileSet TileSet;
        public UnitSet UnitSet;
        public ItemSet ItemSet;

        private List<Tile> _map;

        private void Start()
        {
            GenerateMap();
        }

        //the map is a a list of all the tiles on this level
        private void GenerateMap()
        {
            _map = new List<Tile>();
            var enemyIndex = 1;
            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                var tile = child.GetComponent<Tile>();

                //initialize the occupying unit for this tile if there is one
                var occupyingUnit = child.GetComponentInChildren<Unit>();
                if (occupyingUnit != null)
                {
                    var unitName = occupyingUnit.gameObject.name;
                    if (occupyingUnit is Enemy)
                        unitName += " " + enemyIndex++;
                    occupyingUnit.InitializeUnit(unitName);
                    tile.SetOccupyingUnit(occupyingUnit);
                }

                //initialize the tile's item if there is one
                var item = child.GetComponentInChildren<Item>();
                if (item != null)
                    tile.Item = item;

                _map.Add(tile);
            }
        }

        public Tile FindTileAt(Vector2 position)
        {
            return FindTileAt((int)position.x, (int)position.y);
        }

        public Tile FindTileAt(float x, float y)
        {
            return FindTileAt((int)x, (int)y);
        }

        public Tile FindTileAt(int x, int y)
        {
            return _map.FirstOrDefault(tile => tile.CoordinatesMatch(x, y));
        }

        public Unit GetPlayer()
        {
            return _map.First(t => t.IsOccupiedByPlayer).GetOccupyingUnit();
        }

        public List<Unit> GetEnemies()
        {
            return _map.Where(t => t.IsOccupiedByEnemy).Select(t => t.GetOccupyingUnit()).ToList();
        }

        void OnDrawGizmos()
        {
            //var pos = UnityEngine.Camera.current.transform.position;

            Gizmos.color = LineColor;

            for (var y = -.5f; y <= MapHeight; y += TileSize)
            {
                Gizmos.DrawLine(new Vector2(-.5f, y / TileSize * TileSize),
                                new Vector2(MapWidth - .5f, y / TileSize * TileSize));
            }
            for (var x = -.5f; x <= MapWidth; x += TileSize)
            {
                Gizmos.DrawLine(new Vector2(x / TileSize * TileSize, -.5f),
                                new Vector2(x / TileSize * TileSize, MapHeight - .5f));
            }
        }
    }
}