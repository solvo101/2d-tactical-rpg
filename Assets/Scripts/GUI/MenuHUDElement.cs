﻿using Assets.Scripts.GUI.MenuItems;

namespace Assets.Scripts.GUI
{
    public class MenuHUDElement : HUDElement
    {
        private MenuItem _selectedMenuItem;
        public MenuItem SelectedMenuItem
        {
            get { return _selectedMenuItem; }
            set
            {
                _selectedMenuItem = value;
                UpdateMenuColors();
            }
        }

        public override void Show()
        {
            base.Show();
            SelectedMenuItem = null;
            UpdateMenuColors();
        }

        protected void UpdateMenuColors()
        {
            transform.FindChild("SkillsMenu").GetComponent<SkillsMenuItem>().OnMouseExit();
            transform.FindChild("MoveMenu").GetComponent<MoveMenuItem>().OnMouseExit();
            //transform.FindChild("ItemsMenu").GetComponent<EndMenuItem>().OnMouseExit();
            //transform.FindChild("SummonMenu").GetComponent<EndMenuItem>().OnMouseExit();
            transform.FindChild("EndMenu").GetComponent<EndMenuItem>().OnMouseExit();
        }

    }
}