﻿using System.Collections.Generic;
using Assets.Scripts.GUI.MenuItems;
using Assets.Scripts.Managers;
using Assets.Scripts.Units;
using Assets.Scripts.Units.Skills;
using UnityEngine;

namespace Assets.Scripts.GUI
{
    public class SubMenuHUDElement : HUDElement
    {
        public GameObject SubMenuItemPrefab;
        public GameObject ManaCostPrefab;

        private SubMenuItem _selectedSubMenuItem;
        public SubMenuItem SelectedSubMenuItem
        {
            get { return _selectedSubMenuItem; }
            set
            {
                _selectedSubMenuItem = value;
                UpdateMenuColors();
            }
        }

        public void Show(Unit unit, UnitActions action)
        {
            switch (action)
            {
                case UnitActions.Skills:
                    AddSkillsToSubMenu(unit.Skills);
                    break;
            }
            SelectedSubMenuItem = null;
            base.Show();
        }

        public override void Hide()
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                Destroy(child.gameObject);
            }
            base.Hide();
        }

        protected void UpdateMenuColors()
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                var subMenuItem = child.GetComponent<SubMenuItem>();
                if (subMenuItem != null)
                    subMenuItem.OnMouseExit();
            }
        }

        private void AddSkillsToSubMenu(IEnumerable<Skill> skills)
        {
            var offset = 0f;
            foreach (var skill in skills)
            {
                var skillGameObject = (GameObject)Instantiate(SubMenuItemPrefab);

                skillGameObject.transform.parent = transform;
                skillGameObject.GetComponent<GUIText>().text = skill.Name;

                skillGameObject.transform.position = new Vector3(transform.position.x - .06f, transform.position.y + .12f - offset, .01f);

                skillGameObject.AddComponent<SkillSubMenuItem>().Skill = skill;

                var manaCost = skill.Cost;
                if (manaCost != 0)
                {
                    var manaCostGameObject = (GameObject)Instantiate(ManaCostPrefab);

                    manaCostGameObject.transform.parent = transform;
                    manaCostGameObject.GetComponent<GUIText>().text = manaCost.ToString();

                    manaCostGameObject.transform.position = new Vector3(transform.position.x + 0.065f, transform.position.y + .12f - offset, .01f);
                }

                offset += .04f;
            }
        }
    }
}