﻿using UnityEngine;

namespace Assets.Scripts.GUI
{
    public class HUDElement : MonoBehaviour
    {
        public virtual void Show()
        {
            guiTexture.enabled = true;
            var guiTextures = GetComponentsInChildren<GUIText>();
            foreach (var g in guiTextures)
                g.enabled = true;
            var text = GetComponentsInChildren<GUITexture>();
            foreach (var g in text)
                g.enabled = true;
        }

        public virtual void Hide()
        {
            guiTexture.enabled = false;
            var guiTextures = GetComponentsInChildren<GUIText>();
            foreach (var g in guiTextures)
                g.enabled = false;
            var text = GetComponentsInChildren<GUITexture>();
            foreach (var g in text)
                g.enabled = false;
        }
    }
}