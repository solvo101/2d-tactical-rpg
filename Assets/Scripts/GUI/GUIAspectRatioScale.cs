﻿using UnityEngine;

namespace Assets.Scripts.GUI
{
    public class GUIAspectRatioScale : MonoBehaviour
    {

        public Vector2 ScaleOnRatio = new Vector2(0.1f, 0.1f);
        private Transform _transform;
        private float _widthHeightRatio;

        void Start()
        {
            _transform = transform;
            SetScale();
        }

        private void Update()
        {
            SetScale();
        }

        //call on an event that tells if the aspect ratio changed
        void SetScale()
        {
            //find the aspect ratio
            _widthHeightRatio = (float)Screen.width / Screen.height;

            //Apply the scale. We only calculate y since our aspect ratio is x (width) authoritative: width/height (x/y)
            _transform.localScale = new Vector3(ScaleOnRatio.x, _widthHeightRatio * ScaleOnRatio.y, 1);
        }
    }
}
