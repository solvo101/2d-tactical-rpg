﻿using Assets.Scripts.Units.Skills;

namespace Assets.Scripts.GUI.MenuItems
{
    public class SkillSubMenuItem : SubMenuItem
    {
        public Skill Skill; // the skill that this menu item represents

        protected override bool IsClickable
        {
            get { return GameManager.GetCurrentUnit().Mana >= Skill.Cost; }
        }

        protected override bool IsSelected
        {
            get { return SelectedSubMenuItem == this; }
        }

        private void OnMouseDown()
        {
            if (!IsClickable) return;
            SelectedSubMenuItem = this;
            GameManager.MenuSkillClicked(Skill);
        }
        
        protected void Awake()
        {
            base.Awake();
        }
    }
}