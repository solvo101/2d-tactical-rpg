﻿using Assets.Scripts.Managers;

namespace Assets.Scripts.GUI.MenuItems
{
    public class MoveMenuItem : MenuItem
    {
        protected override bool IsClickable
        {
            get { return !GameManager.CompletedActions.Contains(UnitActions.Move) && guiText.enabled; }
        }

        protected override bool IsSelected
        {
            get { return SelectedMenuItem == this; }
        }

        private void OnMouseDown()
        {
            if (!IsClickable) return;
            GameManager.MenuMoveClicked();
            SelectedMenuItem = this;
        }
    }
}