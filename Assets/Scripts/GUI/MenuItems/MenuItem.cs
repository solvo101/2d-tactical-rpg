﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.GUI.MenuItems
{
    public abstract class MenuItem : MonoBehaviour
    {
        protected abstract bool IsClickable { get; }
        protected abstract bool IsSelected { get; }
        protected MenuItem SelectedMenuItem
        {
            get
            {
                return _parent.SelectedMenuItem;
            }

            set
            {
                _parent.SelectedMenuItem = value;
            }
        }
        protected GameManager GameManager;
        private MenuHUDElement _parent;

        protected void Awake()
        {
            GameManager = FindObjectOfType<GameManager>();
            if (transform.parent != null)
                _parent = transform.parent.GetComponent<MenuHUDElement>();
        }

        private void OnMouseOver()
        {
            guiText.color = IsSelected ? Color.red : IsClickable ? Color.yellow : Color.gray;
        }

        public void OnMouseExit()
        {
            guiText.color = IsSelected ? Color.red : IsClickable ? Color.white : Color.gray;
        }
    }
}