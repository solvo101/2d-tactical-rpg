﻿namespace Assets.Scripts.GUI.MenuItems
{
    public abstract class SubMenuItem : MenuItem
    {
        private SubMenuHUDElement _parent;

        protected SubMenuItem SelectedSubMenuItem
        {
            get
            {
                return _parent.SelectedSubMenuItem;
            }

            set
            {
                _parent.SelectedSubMenuItem = value;
            }
        }

        protected void Awake()
        {
            if (transform.parent != null)
                _parent = transform.parent.GetComponent<SubMenuHUDElement>();
            base.Awake();
        }
    }
}