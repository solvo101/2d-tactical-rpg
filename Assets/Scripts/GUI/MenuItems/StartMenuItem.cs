﻿namespace Assets.Scripts.GUI.MenuItems
{
    public class StartMenuItem : MenuItem
    {
        protected override bool IsClickable
        {
            get { return guiText.enabled; }
        }

        protected override bool IsSelected
        {
            get { return false; }
        }

        private void OnMouseDown()
        {
            if (!IsClickable) return;
            GameManager.BeginGame();
        }
    }
}