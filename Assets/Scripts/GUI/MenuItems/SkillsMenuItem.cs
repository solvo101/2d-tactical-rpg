﻿using Assets.Scripts.Managers;

namespace Assets.Scripts.GUI.MenuItems
{
    public class SkillsMenuItem : MenuItem
    {
        protected override bool IsClickable
        {
            get { return !GameManager.CompletedActions.Contains(UnitActions.Skills) && guiText.enabled; }
        }

        protected override bool IsSelected
        {
            get { return SelectedMenuItem == this; }
        }

        private void OnMouseDown()
        {
            if (!IsClickable || IsSelected) return;
            GameManager.MenuSkillsClicked();
            SelectedMenuItem = this;
        }
    }
}