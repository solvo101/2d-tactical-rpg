﻿using UnityEngine;

namespace Assets.Scripts.Units
{
    public class UnitSet : ScriptableObject
    {
        public Unit[] Units = new Unit[0];
    }
}