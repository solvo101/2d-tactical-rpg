﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Managers;
using Assets.Scripts.Map;
using Assets.Scripts.Units.Attributes;
using Assets.Scripts.Units.Attributes.BuffEffects;
using Assets.Scripts.Units.Skills;
using Assets.Scripts.Units.Skills.SkillEffects;
using UnityEngine;

namespace Assets.Scripts.Units
{
    public abstract class Unit : MonoBehaviour
    {
        protected Tile CurrentDestination;
        protected GameManager GameManager;
        protected MapManager MapManager;
        protected HUDManager HUDManager;
        public TextMesh HealthIndicator;
        public GameObject DamageText;
        public GameObject HealingText;
        public Tile Location;
        protected bool ImMoving;
        protected Queue<Tile> Path;
        public string UnitName;
        protected UnitAttributes Attributes;
        public List<Skill> Skills { get; protected set; }

        public int MoveDistance
        {
            get { return Attributes.MoveDistanceBuffed; }
        }
        public int Damage
        {
            get { return Attributes.DamageBuffed; }
        }
        public int MaxHealth
        {
            get { return Attributes.MaxHealthBuffed; }
        }
        public int Health
        {
            get { return Attributes.HealthBuffed; }
            private set { Attributes.Health = value; }
        }
        public int MaxMana
        {
            get { return Attributes.MaxManaBuffed; }
        }
        public int Mana
        {
            get { return Attributes.ManaBuffed; }
            private set { Attributes.Mana = value; }
        }

        public int MoveDistanceMod
        {
            get { return Attributes.MoveDistanceMod; }
        }
        public int DamageMod
        {
            get { return Attributes.DamageMod; }
        }
        public int MaxHealthMod
        {
            get { return Attributes.MaxHealthMod; }
        }
        public int HealthMod
        {
            get { return Attributes.HealthMod; }
        }
        public int MaxManaMod
        {
            get { return Attributes.MaxHealthMod; }
        }
        public int ManaMod
        {
            get { return Attributes.HealthMod; }
        }

        // Use this for initialization
        protected virtual void Start()
        {
            Attributes = GetComponent<UnitAttributes>();
            GameManager = FindObjectOfType<GameManager>();
            MapManager = FindObjectOfType<MapManager>();
            HUDManager = FindObjectOfType<HUDManager>();
            CurrentDestination = Location = transform.parent.GetComponent<Tile>();
            Skills = new List<Skill>();

            var skill = new Skill("Attack", 1, 0, false);
            skill.AddSkillEffect(new Attack
            {
                SkillDamageMod = 0,
                Skill = skill
            });
            Skills.Add(skill);
        }

        // Update is called once per frame
        protected void Update()
        {
            if (!ImMoving) return;
            if (Location == CurrentDestination)
            {
                SetNextDestination();
                return;
            }
            var destinationLocation = CurrentDestination.GetGridPosition();
            if (Vector2.Distance(destinationLocation, transform.position) > .07f)
            {
                transform.position += (Vector3)(destinationLocation - (Vector2)transform.position).normalized * 5f * Time.deltaTime;
            }
            else
            {
                Location = CurrentDestination;
                Location.SetOccupyingUnit(this);
            }
        }

        protected abstract void TakeTurn();

        public void BeginTurn()
        {
            Attributes.DecrementBuffCounters();
            HUDManager.ShowUnitTooltip(this);
            TakeTurn();
        }

        public void SetNewDestination(Queue<Tile> path)
        {
            Path = path;
            DequeueNextLocation();
            ImMoving = true;
        }

        private void DequeueNextLocation()
        {
            Location.RemoveOccupyingUnit();
            CurrentDestination = Path.Dequeue();
        }

        public virtual void InitializeUnit(string unitName)
        {
            if (MapManager == null)
                Start();
            ImMoving = false;
            Health = MaxHealth;
            UnitName = unitName;
            UpdateHealthIndicator();
        }

        public void UpdateHealthIndicator()
        {
            HealthIndicator.text = Health.ToString();
        }

        public void UseSkill(Unit target, Skill selectedSkill)
        {
            target.ShowHealthIndicator(true);
            Mana -= selectedSkill.Cost;
            HUDManager.ShowUnitTooltip(this);
            if(Mana < 0) throw new Exception("The player used a skill they can't afford!");
            selectedSkill.UseSkill(this, target);
        }

        public void DoneUsingSkill()
        {
            GameManager.DoneUsingSkill();
            if(!GameManager.HealthIndicatorsActive) ShowHealthIndicator(false);
        }

        protected virtual void SetNextDestination()
        {
            if (Path.Any())
                DequeueNextLocation();
            else
            {
                ImMoving = false;
                GameManager.DoneMoving(Location);
            }
        }

        public void TakeHit(int damage)
        {
            //lose health
            Attributes.Health -= damage;
            HealthIndicator.text = Attributes.Health.ToString();

            DisplayFloatingDamageText(damage);

            if (Attributes.Health > 0) return;

            //The unit has died
            Location.RemoveOccupyingUnit();
            Destroy(gameObject);
            GameManager.CheckGameStatus(); // check to see if the game should end now that this unit is dead
        }

        public void ShowHealthIndicator(bool show)
        {
            if (!show && GameManager.HealthIndicatorsActive) return;
            transform.FindChild("pixel_heart").GetComponent<SpriteRenderer>().enabled = show;
            HealthIndicator.renderer.enabled = show;
        }

        public void Heal(int healAmount)
        {
            var newHealth = Attributes.Health + healAmount;
            Attributes.Health = newHealth > Attributes.MaxHealth ? Attributes.MaxHealth : newHealth;
            HealthIndicator.text = Attributes.Health.ToString();
            if(GameManager.IsMyTurn(this))
                HUDManager.ShowUnitTooltip(this);
            DisplayFloatingHealingText(healAmount);

        }

        private void DisplayFloatingDamageText(int amount)
        {
            DisplayFloatingText(DamageText, "-" + amount);
        }

        private void DisplayFloatingHealingText(int amount)
        {
            DisplayFloatingText(HealingText, "+" + amount);
        }

        private void DisplayFloatingText(GameObject prefab, string text)
        {
            var gridPosition = Location.GetGridPosition();
            var floatingTtext = Instantiate(prefab, new Vector3(gridPosition.x, gridPosition.y + 1, 0), Quaternion.identity) as GameObject;
            floatingTtext.GetComponent<FloatingText>().DisplayFloatingText(text);
        }

        public void ApplyBuffEffects(List<BuffEffect> buffEffects)
        {
            Attributes.ApplyBuffEffects(buffEffects);
        }

        public List<BuffEffect> GetActiveBuffs()
        {
            return Attributes.GetActiveBuffs();
        }

        public Tile GetLocation()
        {
            return Location;
        }
    }
}