﻿using System.Collections.Generic;
using Assets.Scripts.Units.Skills.SkillEffects;

namespace Assets.Scripts.Units.Skills
{
    public class Skill
    {
        public string Name { get; private set; }
        public int Range { get; private set; }
        public int Cost { get; private set; }
        public bool CanTargetSelf { get; private set; }
        private List<SkillEffect> Effects { get; set; }
        private int _currentEffect;
        private Unit _user;
        private Unit _target;

        public Skill(string name, int range, int cost, bool canTargetSelf)
        {
            Name = name;
            Range = range;
            Cost = cost;
            CanTargetSelf = canTargetSelf;
            Effects = new List<SkillEffect>();
        }

        public void UseSkill(Unit user, Unit target)
        {
            _user = user;
            _target = target;
            _currentEffect = 0;
            NextEffect();
        }

        public void AddSkillEffect(SkillEffect skillEffect)
        {
            Effects.Add(skillEffect);
        }

        public void DoneWithSkillEffect()
        {
            _currentEffect++;
            if (_currentEffect == Effects.Count)
                _user.DoneUsingSkill();
            else
                NextEffect();
        }

        private void NextEffect()
        {
            Effects[_currentEffect].Activate(_user, _target);
        }
    }
}
