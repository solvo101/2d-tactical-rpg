﻿namespace Assets.Scripts.Units.Skills.SkillEffects
{
    public class Heal : SkillEffect
    {
        public int HealAmount { get; set; }

        public override void Activate(Unit user, Unit target)
        {
            if (user != null && target != null)
            {
                target.Heal(HealAmount);
            }

            Skill.DoneWithSkillEffect();
        }
    }
}
