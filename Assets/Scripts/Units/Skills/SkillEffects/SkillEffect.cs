﻿namespace Assets.Scripts.Units.Skills.SkillEffects
{
    public abstract class SkillEffect
    {
        public abstract void Activate(Unit user, Unit target);
        public Skill Skill; //the skill that this effect is attached to

        public void DoneWithSkillEffect()
        {
            Skill.DoneWithSkillEffect();
        }
    }
}
