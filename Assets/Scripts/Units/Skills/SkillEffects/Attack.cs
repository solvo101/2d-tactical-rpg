﻿namespace Assets.Scripts.Units.Skills.SkillEffects
{
    public class Attack : SkillEffect
    {
        public int SkillDamageMod;

        public override void Activate(Unit user, Unit target)
        {
            if (user == null || target == null)
            {
                Skill.DoneWithSkillEffect();
                return;
            }

            user.GetComponent<AttackingUnit>().BeginAttackAnimation(target, user.Damage + SkillDamageMod, this);
        }
    }
}
