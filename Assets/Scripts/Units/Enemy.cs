﻿using System;
using System.Linq;
using Assets.Scripts.Units.Attributes;
using UnityEngine;

namespace Assets.Scripts.Units
{
    public class Enemy : Unit
    {
        private Player _target;
        public int AggroRange;
        public bool IsActivated { get; set; }

        public bool HasTarget
        {
            get { return _target != null; }
        }

        // Use this for initialization
        private new void Start()
        {
            IsActivated = false;
            base.Start();
        }

        public override void InitializeUnit(string unitName)
        {
            base.InitializeUnit(unitName);
            //_target = GameManager.GetPlayer();
        }

        protected override void TakeTurn()
        {
            
            if (HasTarget)
            {
                if (!IsNextToTarget())
                {
                    GameManager.MoveCurrentUnit(_target.GetLocation());
                }
                else
                {
                    DecideToAttack();
                }
            }
            else
            {
                //look for target in aggro range
                var tiles = GameManager.GetTilesInRange(Location, AggroRange);
                var targetTile = tiles.FirstOrDefault(t => t.IsOccupiedByPlayer);
                if (targetTile != null)
                {
                    _target = (Player) targetTile.GetOccupyingUnit();
                    IsActivated = true;

                    ToggleSleepIcon(false);

                    TakeTurn();
                }
                else
                {
                    GameManager.EndTurn();
                }
            }
        }

        private void ToggleSleepIcon(bool enable)
        {
            var sleepAnimation = transform.Find("SleepAnimation").gameObject;
            sleepAnimation.GetComponent<SpriteRenderer>().enabled = enable;
            sleepAnimation.GetComponent<Animator>().enabled = enable;
        }

        public void DecideToAttack()
        {
            if (IsNextToTarget())
            {
                GameManager.UseSkillAt(_target.GetLocation(), Skills.First());
            }
            else
            {
                GameManager.EndTurn();
            }
        }

        private bool IsNextToTarget()
        {
            var targetPosition = _target.GetLocation().GetGridPosition();
            var enemyPosition = Location.GetGridPosition();

            return Math.Abs(enemyPosition.x - targetPosition.x) == 1 &&
                   Math.Abs(enemyPosition.y - targetPosition.y) == 0 ||
                   Math.Abs(enemyPosition.y - targetPosition.y) == 1 &&
                   Math.Abs(enemyPosition.x - targetPosition.x) == 0;
        }
    }
}