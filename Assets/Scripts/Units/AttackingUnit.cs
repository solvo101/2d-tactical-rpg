﻿using Assets.Scripts.Units.Skills;
using Assets.Scripts.Units.Skills.SkillEffects;
using UnityEngine;

namespace Assets.Scripts.Units
{
    public class AttackingUnit : MonoBehaviour
    {
        private const float AttackSpeed = 25f;
        private const float ReturnSpeed = 8f;
        private int _damage;
        private bool _isAttacking;
        private bool _isReturning;
        private Vector2 _originalLocation;
        private Unit _target;
        private SkillEffect _skillEffect;

        private void Update()
        {
            if (_isAttacking)
            {
                transform.position = Vector3.Lerp(transform.position, _target.transform.position,
                    Time.deltaTime*AttackSpeed);
                if (Vector2.Distance(_target.transform.position, transform.position) < .007f)
                {
                    _isAttacking = false;
                    _isReturning = true;
                    _target.TakeHit(_damage);
                }
            }
            if (_isReturning)
            {
                transform.position = Vector3.Lerp(transform.position, _originalLocation, Time.deltaTime*ReturnSpeed);
                if (Vector2.Distance(_originalLocation, transform.position) < .007f)
                {
                    _isAttacking = false;
                    _isReturning = false;
                    _skillEffect.DoneWithSkillEffect();
                    if (_target != null)
                        _target.ShowHealthIndicator(false);
                }
            }
        }

        public void BeginAttackAnimation(Unit target, int damage, SkillEffect skillEffect)
        {
            _originalLocation = transform.position;
            _damage = damage;
            _skillEffect = skillEffect;
            _target = target;
            _isAttacking = true;
        }
    }
}