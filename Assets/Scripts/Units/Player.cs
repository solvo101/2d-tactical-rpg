﻿using Assets.Scripts.Units.Skills;
using Assets.Scripts.Units.Skills.SkillEffects;

namespace Assets.Scripts.Units
{
    public class Player : Unit
    {
        protected override void Start()
        {
            base.Start();

            var skill = new Skill("Bash", 1, 1, false);
            skill.AddSkillEffect(new Attack
            {
                SkillDamageMod = 1,
                Skill = skill
            });
            Skills.Add(skill);

            skill = new Skill("D-Strike", 1, 3, false);
            skill.AddSkillEffect(new Attack
            {
                SkillDamageMod = 0,
                Skill = skill
            });
            skill.AddSkillEffect(new Attack
            {
                SkillDamageMod = 0,
                Skill = skill
            });
            Skills.Add(skill);

            skill = new Skill("Heal", 3, 2, true);
            skill.AddSkillEffect(new Heal
            {
                HealAmount = 3,
                Skill = skill
            });
            Skills.Add(skill);
            
            skill = new Skill("PPT", 10, 5, true);
            skill.AddSkillEffect(new Attack
            {
                SkillDamageMod= 9999997,
                Skill = skill
            });
            Skills.Add(skill);
        }
        protected override void TakeTurn()
        {
        }
    }
}