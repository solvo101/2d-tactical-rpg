﻿namespace Assets.Scripts.Units.Attributes.BuffEffects
{
    public class MovementEffect : BuffEffect
    {
        public override void Apply(UnitAttributes unitAttributes)
        {
            unitAttributes.MoveDistanceMod += Value;
        }

        public override void Remove(UnitAttributes unitAttributes)
        {
            unitAttributes.MoveDistanceMod -= Value;
        }
    }
}
