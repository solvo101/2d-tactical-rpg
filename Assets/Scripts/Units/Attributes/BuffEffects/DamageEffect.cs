﻿namespace Assets.Scripts.Units.Attributes.BuffEffects
{
    public class DamageEffect : BuffEffect
    {
        public override void Apply(UnitAttributes unitAttributes)
        {
            unitAttributes.DamageMod += Value;
        }

        public override void Remove(UnitAttributes unitAttributes)
        {
            unitAttributes.DamageMod -= Value;
        }
    }
}
