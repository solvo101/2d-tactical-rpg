﻿using System.Reflection;

namespace Assets.Scripts.Units.Attributes.BuffEffects
{
    public abstract class BuffEffect
    {
        public int Duration { get; set; }
        public int Value { get; set; }

        public abstract void Apply(UnitAttributes unitAttributes);
        public abstract void Remove(UnitAttributes unitAttributes);
    }
}
