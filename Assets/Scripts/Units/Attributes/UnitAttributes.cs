﻿using System.Collections.Generic;
using Assets.Scripts.Units.Attributes.BuffEffects;
using UnityEngine;

namespace Assets.Scripts.Units.Attributes
{
    public class UnitAttributes : MonoBehaviour
    {
        public int MoveDistance; //the base stat
        public int MoveDistanceMod { get; set; } //The modifier that changes this stat from buffs/debuffs
        public int MoveDistanceBuffed { get { return MoveDistance + MoveDistanceMod; } } //The modified stat

        public int Damage;
        public int DamageMod { get; set; }
        public int DamageBuffed { get { return Damage + DamageMod; } }

        public int MaxHealth;
        public int MaxHealthMod { get; set; }
        public int MaxHealthBuffed { get { return MaxHealth + MaxHealthMod; } }

        public int Health;
        public int HealthMod { get; set; }
        public int HealthBuffed { get { return Health + HealthMod; } }
        
        public int MaxMana;
        public int MaxManaMod { get; set; }
        public int MaxManaBuffed { get { return MaxMana + MaxManaMod; } }

        public int Mana;
        public int ManaMod { get; set; }
        public int ManaBuffed { get { return Mana + ManaMod; } }

        private List<BuffEffect> ActiveBuffs { get; set; }

        void Start()
        {
            ActiveBuffs = new List<BuffEffect>();
        }

        public void ApplyBuffEffects(List<BuffEffect> buffEffects)
        {
            buffEffects.ForEach(b => b.Apply(this));
            ActiveBuffs.AddRange(buffEffects);
        }

        public void DecrementBuffCounters()
        {
            var removeBuffs = new List<BuffEffect>();//list of buffs to remove because their duration is up
            foreach (var buffEffect in ActiveBuffs)
            {
                buffEffect.Duration--;
                if (buffEffect.Duration == 0)
                    removeBuffs.Add(buffEffect);
            }
            foreach (var buffEffect in removeBuffs)
            {
                buffEffect.Remove(this);
                ActiveBuffs.Remove(buffEffect);
            }
        }

        public List<BuffEffect> GetActiveBuffs()
        {
            return ActiveBuffs;
        }
    }
}
