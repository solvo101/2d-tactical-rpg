﻿using UnityEngine;

namespace Assets.Scripts.Units
{
    public class FloatingText : MonoBehaviour
    {
        public float MoveSpeed = 2f;
        private Vector2 _endPosition;
        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
            transform.position = Vector3.Lerp(transform.position, _endPosition, Time.deltaTime*MoveSpeed);
            if (!(Vector2.Distance(_endPosition, transform.position) < .07f)) return;
            Destroy(gameObject);
        }

        public void DisplayFloatingText(string text)
        {
            gameObject.GetComponent<Renderer>().enabled = true;
            gameObject.GetComponent<TextMesh>().text = text;
            _endPosition = transform.position;
            _endPosition.y += 2;
        }
    }
}