﻿using UnityEngine;

namespace Assets.Scripts.Animations
{
    public class Animation : MonoBehaviour
    {
        public float Speed = 1f;

        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
            GetComponent<Animator>().speed = Speed;
        }
    }
}