﻿using UnityEngine;

namespace Assets.Scripts
{
    public class TileSet : ScriptableObject
    {
        public Transform[] TilePrefabs = new Transform[0];
    }
}