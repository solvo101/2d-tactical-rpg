﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Managers;

namespace Assets.Scripts.Map
{
    public class Pathfinder
    {
        private readonly MapManager _mapManager;

        public Pathfinder(MapManager mapManager)
        {
            _mapManager = mapManager;
        }

        public List<Tile> CreatePath(Tile startTile, Tile endTile)
        {
            var path = FindPath(startTile, endTile);
            if (path == null) return null;
            path.Reverse(); //the list was generated backwards
            path.RemoveAt(0); //drop the first node because the unit is standing on it
            return path;
        }

        public bool IsPathAvailable(Tile startTile, Tile endTile, int distance)
        {
            var path = FindPath(startTile, endTile);
            if (path == null) return false;
            return path.Count - 2 < distance;
        }

        private List<Tile> FindPath(Tile startTile, Tile endTile)
        {
            var openNodes = new List<Node>();
            var closedNodes = new List<Node>();

            //make the starting node
            var node = new Node(endTile, startTile, null);
            openNodes.Add(node);

            //perform A*
            while (node.Tile != endTile)
            {
                openNodes.Remove(node);
                closedNodes.Add(node);
                AddAvailableAdjacentNodes(endTile, openNodes, closedNodes, node);
                if (!openNodes.Any())
                    return null; // no path could be found
                node = openNodes.Aggregate((n1, n2) => n1.Score < n2.Score ? n1 : n2);
            }
            //Once we're here, node will reference the ending tile (last node on the path)

            //build the path
            var path = new List<Tile> {node.Tile};

            var parent = node.ParentNode;
            while (parent != null)
            {
                path.Add(parent.Tile);
                parent = parent.ParentNode;
            }
            return path;
        }

        private void AddAvailableAdjacentNodes(Tile endTile, List<Node> openNodes, List<Node> closedNodes,
            Node parentNode)
        {
            var gridPosition = parentNode.Tile.GetGridPosition();
            AddNode(endTile, openNodes, closedNodes, parentNode, gridPosition.x + 1, gridPosition.y); //right
            AddNode(endTile, openNodes, closedNodes, parentNode, gridPosition.x - 1, gridPosition.y); //left
            AddNode(endTile, openNodes, closedNodes, parentNode, gridPosition.x, gridPosition.y + 1); //up
            AddNode(endTile, openNodes, closedNodes, parentNode, gridPosition.x, gridPosition.y - 1); //down
        }

        private void AddNode(Tile endTile, List<Node> openNodes, List<Node> closedNodes, Node parentNode, float x,
            float y)
        {
            var tile = _mapManager.FindTileAt(x, y);
            if (tile == null || tile.IsBarrier || tile.IsOccupiedByEnemy || closedNodes.Exists(n => n.Tile == tile))
                return;

            var existingNode = openNodes.FirstOrDefault(n => n.Tile == tile); //Finds this tile if it already exists 
            var newNode = new Node(endTile, tile, parentNode);
            if (existingNode != null && newNode.MovementCost < existingNode.MovementCost)
            {
                existingNode.Calculate(newNode);
            }
            if (existingNode == null)
                openNodes.Add(newNode);
        }
    }

    internal class Node
    {
        private const int Cost = 10;
        public Tile EndTile;
        public int Heuristic; //H
        public int MovementCost; //G
        public Node ParentNode;
        public int Score; //F
        public Tile Tile;

        public Node(Tile endTile, Tile tile, Node pNode)
        {
            EndTile = endTile;
            Tile = tile;
            Calculate(pNode);
        }

        public void Calculate(Node parentNode)
        {
            ParentNode = parentNode;
            MovementCost = parentNode == null ? 0 : parentNode.MovementCost + Cost;
            var xCount = Math.Abs(EndTile.GetGridPosition().x - Tile.GetGridPosition().x);
            var yCount = Math.Abs(EndTile.GetGridPosition().y - Tile.GetGridPosition().y);
            Heuristic = (int) (xCount + yCount)*Cost;
            Score = MovementCost + Heuristic;
        }
    }
}