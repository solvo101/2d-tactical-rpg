﻿using System;
using Assets.Scripts.Items;
using Assets.Scripts.Managers;
using Assets.Scripts.Units;
using UnityEngine;

namespace Assets.Scripts.Map
{
    public class Tile : MonoBehaviour
    {
        public GameObject ChoiceTileAnimation;
        public bool Debug = true;
        public GameObject DestinationTileAnimation;
        public GameObject HighlightTileAnimation;
        public bool IsBarrier = false;
        public Vector2 GridPosition;
        private Unit _occupyingUnit;
        public Item Item;
        private GameManager _gameManager;
        private bool _isChoice;
        private bool _isDestination;

        public bool IsDestination
        {
            set
            {
                SetAnimation(DestinationTileAnimation, value);
                _isDestination = value;
            }
            get { return _isDestination; }
        }

        public bool IsOccupied
        {
            get { return _occupyingUnit != null; }
        }

        public bool IsOccupiedByEnemy
        {
            get { return _occupyingUnit is Enemy; }
        }

        public bool IsOccupiedByPlayer
        {
            get { return _occupyingUnit is Player; }
        }

        public bool IsAvailable
        {
            get { return !IsBarrier && !IsOccupied; }
        }

        public bool IsChoice
        {
            get { return _isChoice; }
            set
            {
                _isChoice = value;
                OnMouseExit();
            }
        }

        private bool IsPlayerTurn
        {
            get { return _gameManager.IsPlayerTurn; }
        }

        private bool IsActionAvailable
        {
            get { return IsPlayerTurn && !IsBarrier; }
        }

        private void SetAnimation(GameObject tileAnimation, bool isActive)
        {
            ChoiceTileAnimation.renderer.enabled = false;
            ChoiceTileAnimation.GetComponent<Animator>().enabled = false;

            DestinationTileAnimation.renderer.enabled = false;
            DestinationTileAnimation.GetComponent<Animator>().enabled = false;

            HighlightTileAnimation.renderer.enabled = false;
            HighlightTileAnimation.GetComponent<Animator>().enabled = false;

            tileAnimation.renderer.enabled = isActive;
            tileAnimation.GetComponent<Animator>().enabled = isActive;
        }

        // Use this for initialization
        private void Start()
        {
            if (Debug)
                transform.FindChild("Coordinates").GetComponent<TextMesh>().renderer.enabled = true;
            _gameManager = FindObjectOfType<GameManager>();
        }

        //private void OnMouseOver()
        //{
        //    if (!_gameManager.IsPlayerTurn || IsOccupied) return;
        //    if (Input.GetMouseButtonDown(1))
        //    {
        //        if (IsBarrier)
        //        {
        //            ((SpriteRenderer) renderer).sprite = WoodTile;
        //            SetAnimation(HighlightTileAnimation, true);
        //            IsBarrier = false;
        //        }
        //        else
        //        {
        //            ((SpriteRenderer) renderer).sprite = MetalTile;
        //            SetAnimation(HighlightTileAnimation, false);
        //            IsBarrier = true;
        //        }
        //    }
        //}

        private void OnMouseEnter()
        {
            SetAnimation(HighlightTileAnimation, IsActionAvailable);
            if (IsOccupied)
            {
                _gameManager.ShowHighlightTooltip(_occupyingUnit);
            }
        }

        private void OnMouseExit()
        {
            if (IsActionAvailable)
                SetAnimation(ChoiceTileAnimation, _isChoice);
            if (IsOccupied)
            {
                _gameManager.HideHighlightTooltip();
            }
        }

        private void OnMouseDown()
        {
            if (IsActionAvailable && IsChoice)
                _gameManager.TileClicked(this);
        }

        public void SetGridPosition(int x, int y)
        {
            GridPosition.x = x;
            GridPosition.y = y;
            transform.FindChild("Coordinates").GetComponent<TextMesh>().text = x + " , " + y;
        }

        public Vector2 GetGridPosition()
        {
            return GridPosition;
        }

        public bool CoordinatesMatch(int x, int y)
        {
            return GridPosition.x == x && GridPosition.y == y;
        }

        public Unit GetOccupyingUnit()
        {
            return _occupyingUnit;
        }

        public void SetOccupyingUnit(Unit unit)
        {
            if (unit == null)
                throw new Exception("Caller didn't provide a unit to set");

            if (_occupyingUnit != null)
                throw new Exception("Unit tried to enter occupied tile");

            _occupyingUnit = unit;

            if (!(Item is Buff)) return;

            (Item as Buff).Activate(unit);
            Item = null;
        }

        public void RemoveOccupyingUnit()
        {
            if (_occupyingUnit == null)
                throw new Exception("Unit tried RemoveOccupyingUnit when it was already null");

            _occupyingUnit = null;
        }
    }
}