﻿using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomEditor(typeof (ParticleSystem))]
    public class ParticleSystemRendererSortingLayersEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var particleSystem = target as ParticleSystem;

            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            var name = EditorGUILayout.TextField("Sorting Layer Name", particleSystem.renderer.sortingLayerName);
            if (EditorGUI.EndChangeCheck())
            {
                particleSystem.renderer.sortingLayerName = name;
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            var order = EditorGUILayout.IntField("Sorting Order", particleSystem.renderer.sortingOrder);
            if (EditorGUI.EndChangeCheck())
            {
                particleSystem.renderer.sortingOrder = order;
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}