﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Managers;
using Assets.Scripts.Map;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomEditor(typeof(Tile))]
    public class TileEditor : UnityEditor.Editor
    {
        private Tile _tile;
        private MapManager _mapManager;

        private void OnEnable()
        {
            _tile = (Tile)target;
            var parent = _tile.transform.parent;
            if (parent != null)
            {
                _mapManager = parent.GetComponent<MapManager>();
            }
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (_mapManager == null || _mapManager.UnitSet == null) return;

            CreateDrowdown("Occupying Unit", _mapManager.UnitSet.Units);
            CreateDrowdown("Item", _mapManager.ItemSet.Items);
        }

        private void CreateDrowdown<T>(string fieldName, IList<T> objectSet)
            where T : MonoBehaviour
        {
            var names = new string[objectSet.Count + 1];
            var values = new int[names.Length];
            names[0] = "None";
            values[0] = 0;
            for (var i = 1; i < names.Length; i++)
            {
                names[i] = objectSet[i - 1] != null ? objectSet[i - 1].name : "";
                values[i] = i;
            }

            var oldIndex = 0;
            var unit = _tile.transform.GetComponentInChildren<T>();
            if (unit != null)
            {
                var unitSetList = objectSet.ToList();
                oldIndex = unitSetList.FindIndex(g => g.gameObject.name.Equals(unit.gameObject.name)) + 1;
            }
            EditorGUI.BeginChangeCheck();
            var index = EditorGUILayout.IntPopup(fieldName, oldIndex, names, values);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Grid Changed");
                if (index == 0)
                {
                    if (unit != null)
                        DestroyImmediate(unit.gameObject);
                }
                else
                {
                    var prefab = objectSet[index - 1];
                    var gameObject = (GameObject)PrefabUtility.InstantiatePrefab(prefab.gameObject);
                    gameObject.transform.position = _tile.transform.position;
                    gameObject.transform.parent = _tile.transform;
                }
            }
        }

    }
}