﻿using System;
using System.IO;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.Items;
using Assets.Scripts.Managers;
using Assets.Scripts.Map;
using Assets.Scripts.Units;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomEditor(typeof (MapManager))]
    public class GridEditor : UnityEditor.Editor
    {
        private MapManager _mapManager;
        private static int TileSize { get { return MapManager.TileSize; } }

        private void OnEnable()
        {
            _mapManager = (MapManager) target;
        }

        [MenuItem("Assets/Create/TileSet")]
        private static void CreateTileSet()
        {
            var asset = CreateInstance<TileSet>();
            var path = AssetDatabase.GetAssetPath(Selection.activeObject);

            if (string.IsNullOrEmpty(path))
            {
                path = "Assets";
            }
            else if (Path.HasExtension(path))
            {
                path = path.Replace(Path.GetFileName(path), "");
            }
            else
            {
                path += "/";
            }

            var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "NewTileSet.asset");
            AssetDatabase.CreateAsset(asset, assetPathAndName);
            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow(); // Focus view on Project Window
            Selection.activeObject = asset;
            //asset.hideFlags = HideFlags.DontSave; //Object will not be destroyed on scene change
        }

        public override void OnInspectorGUI()
        {
            //Line Color
            GUILayout.BeginHorizontal();
            GUILayout.Label("Line Color");
            _mapManager.LineColor = EditorGUILayout.ColorField(_mapManager.LineColor, GUILayout.Width(200));
            GUILayout.EndHorizontal();

            //Map Width
            GUILayout.BeginHorizontal();
            GUILayout.Label("Map Width");
            _mapManager.MapWidth = (int) EditorGUILayout.Slider(_mapManager.MapWidth, 5, 100);
            GUILayout.EndHorizontal();

            //Map Height
            GUILayout.BeginHorizontal();
            GUILayout.Label("Map Height");
            _mapManager.MapHeight = (int)EditorGUILayout.Slider(_mapManager.MapHeight, 5, 100);
            GUILayout.EndHorizontal();

            //Tile Prefab
            EditorGUI.BeginChangeCheck();
            var newTilePrefab =
                (Transform) EditorGUILayout.ObjectField("Tile Prefab", _mapManager.TilePrefab, typeof (Transform), false);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Grid Changed");
                _mapManager.TilePrefab = newTilePrefab;
            }

            //Unit Set
            EditorGUI.BeginChangeCheck();
            var newUnitSet = (UnitSet)EditorGUILayout.ObjectField("Unit Set", _mapManager.UnitSet, typeof(UnitSet), false);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Grid Changed");
                _mapManager.UnitSet = newUnitSet;
            }

            //Tile Map
            EditorGUI.BeginChangeCheck();
            var newTileSet = (TileSet) EditorGUILayout.ObjectField("Tile Set", _mapManager.TileSet, typeof (TileSet), false);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Grid Changed");
                _mapManager.TileSet = newTileSet;
            }

            if (_mapManager.TileSet != null)
            {
                var names = new string[_mapManager.TileSet.TilePrefabs.Length];
                var values = new int[names.Length];

                for (var i = 0; i < names.Length; i++)
                {
                    names[i] = _mapManager.TileSet.TilePrefabs[i] != null ? _mapManager.TileSet.TilePrefabs[i].name : "";
                    values[i] = i;
                }

                var oldIndex = 0;
                var currentTilePrefab = _mapManager.TilePrefab;
                if (currentTilePrefab != null)
                {
                    var tileSetList = _mapManager.TileSet.TilePrefabs.ToList();
                    oldIndex = tileSetList.FindIndex(t => t == currentTilePrefab);
                }
                EditorGUI.BeginChangeCheck();
                var index = EditorGUILayout.IntPopup("Select Tile", oldIndex, names, values);

                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(target, "Grid Changed");
                    _mapManager.TilePrefab = _mapManager.TileSet.TilePrefabs[index];
                }
            }

            //Item Set
            EditorGUI.BeginChangeCheck();
            var newItemSet = (ItemSet)EditorGUILayout.ObjectField("Item Set", _mapManager.ItemSet, typeof(ItemSet), false);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Grid Changed");
                _mapManager.ItemSet = newItemSet;
            }
        }

        private void OnSceneGUI()
        {
            var controlId = GUIUtility.GetControlID(FocusType.Passive);
            var e = Event.current;
            if (e.isMouse && e.type == EventType.MouseDown)
            {
                GUIUtility.hotControl = controlId;
                e.Use();
                var mousePos = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight)).origin;

                if (mousePos.x < -0.5f || mousePos.x > _mapManager.MapWidth - 0.5f ||
                    mousePos.y < -0.5f || mousePos.y > _mapManager.MapHeight - 0.5f) return;

                if (e.button == 0) // left click
                {
                    var prefab = _mapManager.TilePrefab;
                    if (prefab)
                    {
                        Undo.IncrementCurrentGroup();
                        var allignedPos = GetAllignedPosition(mousePos);
                        if (GetTransformFromPosition(allignedPos) != null) return;
                        var gameObject = (GameObject)PrefabUtility.InstantiatePrefab(prefab.gameObject);
                        gameObject.transform.position = allignedPos;
                        gameObject.transform.parent = _mapManager.transform;
                        gameObject.GetComponent<Tile>().SetGridPosition((int)(allignedPos.x), (int)(allignedPos.y));
                        Undo.RegisterCreatedObjectUndo(gameObject, "Create" + gameObject.name);
                    }
                }
                else if (e.button == 1) //right click
                {
                    var transform = GetTransformFromPosition(GetAllignedPosition(mousePos));
                    if (transform != null)
                        DestroyImmediate(transform.gameObject);
                }
            }

            if (e.isMouse && e.type == EventType.mouseUp)
            {
                GUIUtility.hotControl = 0;
            }
        }

        private static Vector2 GetAllignedPosition(Vector3 mousePos)
        {
            return new Vector2(GetAllignedDimension(mousePos.x), GetAllignedDimension(mousePos.y)) - new Vector2(0.5f, 0.5f);
        }

        private static float GetAllignedDimension(float x)
        {
            x -= 0.5f;
            return (float) (Math.Floor(x/TileSize)*TileSize + 1.5f);
        }

        private Transform GetTransformFromPosition(Vector3 position)
        {
            var i = 0;
            while (i < _mapManager.transform.childCount)
            {
                var transform = _mapManager.transform.GetChild(i);
                if (transform.position == position)
                {
                    return transform;
                }
                i++;
            }
            return null;
        }
    }
}