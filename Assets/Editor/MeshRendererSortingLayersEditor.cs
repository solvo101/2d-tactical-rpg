﻿using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomEditor(typeof (MeshRenderer))]
    public class MeshRendererSortingLayersEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var renderer = target as MeshRenderer;

            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            var name = EditorGUILayout.TextField("Sorting Layer Name", renderer.sortingLayerName);
            if (EditorGUI.EndChangeCheck())
            {
                renderer.sortingLayerName = name;
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            var order = EditorGUILayout.IntField("Sorting Order", renderer.sortingOrder);
            if (EditorGUI.EndChangeCheck())
            {
                renderer.sortingOrder = order;
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}