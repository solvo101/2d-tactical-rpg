﻿using UnityEngine;

public class CameraControls : MonoBehaviour
{

    public float Sensitivity;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float move = camera.orthographicSize;
        float speed = Input.GetAxis("Mouse ScrollWheel");
        move += speed * Sensitivity;
        camera.orthographicSize = move;
    }



}